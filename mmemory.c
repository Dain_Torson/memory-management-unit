#include "mmemory.h"

#ifdef _WIN32
	LARGE_INTEGER frequency;
	LARGE_INTEGER time1, time2;           
#endif 

VA physicMem;
VA driveMem;
struct page * pages;

char initialized = 0;
unsigned int numOfPages = 0;
unsigned int pageSize = 0;
unsigned int pageFaults = 0;
unsigned int shift = 0;
double addrCalculationTime = 0;

int virtualToBlockParams(VA ptr, int * pageNum, int * blockOffset) {

	if (ptr == NULL) return -1;

#ifdef _WIN32
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&time1);
#endif

	int vAdd = (int)ptr - 1;

	*(pageNum) = vAdd >> shift;
	*(blockOffset) = vAdd - (*(pageNum) << shift);

#ifdef _WIN32
	QueryPerformanceCounter(&time2);
	addrCalculationTime += (time2.QuadPart - time1.QuadPart) * 1000.0 / frequency.QuadPart;
#endif

	return 0;
}

VA generateVirtualAdress(int pageNum, int blockOffset) {
	
#ifdef _WIN32
	QueryPerformanceFrequency(&frequency);
	QueryPerformanceCounter(&time1);
#endif

	VA ptr = NULL;

	int address = (pageNum << shift) + blockOffset;
	ptr = ptr + address + 1;

#ifdef _WIN32
	QueryPerformanceCounter(&time2);
	addrCalculationTime += (time2.QuadPart - time1.QuadPart) * 1000.0 / frequency.QuadPart;
#endif

	return ptr;
}

struct block * createEmptyBlock(unsigned int size) {
	struct block * emptyBlock = (struct block *) malloc(sizeof(struct block));
	emptyBlock->pNext = NULL;
	emptyBlock->szBlock = size;
	emptyBlock->free = 1;
	emptyBlock->offset = 0;
	return emptyBlock;
}

int findLeastValuablePage(int * idx) {

	int age = 0;

	for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
		if (pages[pageIdx].info->inUse == 1 && pages[pageIdx].info->age > age) {
			age = pages[pageIdx].info->age;
			*(idx) = pageIdx;
		}
	}

	if (age == 0) return -1;
	return 0;
}

void copyTodrive(struct page * targetPage, int idx) {
	for (int byte = 0; byte < pageSize; ++byte) {
		driveMem[idx*pageSize + byte] = physicMem[targetPage->info->offsetPage*pageSize + byte];
	}
}

void copyToPhysicMem(struct page * targetPage, int idx) {
	for (int byte = 0; byte < pageSize; ++byte) {
		physicMem[targetPage->info->offsetPage*pageSize + byte] = driveMem[idx*pageSize + byte];
	}
}

void tick() {
	for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
		if (pages[pageIdx].info->inUse == 1) {
			pages[pageIdx].info->age += 1;
		}
	}
}

int loadPage(int pageIdx) {

	if (pages[pageIdx].info->inUse == 1) {
		return -1;
	}

	int idx = 0;
	int err = findLeastValuablePage(&idx);
	if (err == -1) return -2;

	int offset = pages[idx].info->offsetPage;
	if (pages[idx].info->modified == 1) {
		copyTodrive(&pages[idx], idx);
		pages[idx].info->modified = 0;
	}
	
	pages[idx].info->inUse = 0;
	pages[idx].info->age = 0;
	pages[idx].info->offsetPage = -1;

	pages[pageIdx].info->inUse = 1;
	pages[pageIdx].info->offsetPage = offset;

	copyToPhysicMem(&pages[pageIdx], pageIdx);

	pageFaults += 1;

	return 0;
}

int initPages(int num, int szPage) {

	if (pages == NULL) {
		_clear();
		return 1;
	}

	if (szPage < 1 || num < 1) {
		_clear();
		return -1;
	}

	pageSize = szPage;
	shift = (int)(ceil(log((double)pageSize) / log(2.0)));
	pageFaults = 0;

	for (int pageIdx = 0; pageIdx < num; ++pageIdx) {
		
		struct block * emptyBlock = createEmptyBlock(szPage);

		pages[pageIdx].maxSizeFreeBlock = szPage;
		pages[pageIdx].pFirstFree = emptyBlock;
		pages[pageIdx].pageShift = 0;
		pages[pageIdx].pFirstUse = NULL;

		struct pageInfo * info = (struct pageInfo *)malloc(sizeof(struct pageInfo));
		info->inUse = (pageIdx + 1) * szPage <= PHYSIC_MEM_SIZE ? 1 : 0;
		info->offsetPage = -1;
		if (info->inUse == 1) {
			info->offsetPage = pageIdx;
		}
		info->age = 0;
		info->modified = 0;

		pages[pageIdx].info = info;
		numOfPages = pageIdx + 1;

		if (emptyBlock == NULL || info == NULL) {
			_clear();
			return 1;
		}
	}

	initialized = 1;
	return 0;
}


int _init(int n, int szPage) {

	if (initialized) {
		_clear();
	}

	physicMem = (VA) malloc(PHYSIC_MEM_SIZE);
	driveMem = (VA)malloc(DRIVE_MEM_SIZE);
	pages = (struct page *)malloc(sizeof( struct page)* n);

	return initPages(n, szPage);
}

int findFirstFreeBlock(struct block * firstFree, size_t size) {

	while (firstFree->szBlock < size || firstFree->free == 0) {
		firstFree = firstFree->pNext;
		if (firstFree == NULL) {
			return -1;
		}
	}
	return 0;
}

struct block * findFirstBlock(struct page * targetPage) {

	struct block * blockPtr = targetPage->pFirstFree;

	if (targetPage->pFirstUse == NULL) {
		return blockPtr;
	}

	if (blockPtr == NULL) {
		return targetPage->pFirstUse;
	}

	while (1) {
		blockPtr = blockPtr->pNext;
		if (blockPtr == NULL) {
			blockPtr = targetPage->pFirstUse;
			break;
		}
		else if (blockPtr == targetPage->pFirstUse) {
			blockPtr = targetPage->pFirstFree;
			break;
		}
	}

	return blockPtr;
}

struct block * createAllocBlock(struct block * freeBlock, struct block * firstBlock, size_t szBlock) {

	struct block * newBlock;

	if (freeBlock->szBlock > szBlock) {
		newBlock = (struct block *) malloc(sizeof(struct block));
		
		if (firstBlock != freeBlock) {
			while (firstBlock->pNext != freeBlock) {
				firstBlock = firstBlock->pNext;
			}
			firstBlock->pNext = newBlock;
		}

		newBlock->pNext = freeBlock;
		newBlock->szBlock = szBlock;
		freeBlock->szBlock -= szBlock;
	}
	else {
		newBlock = freeBlock;
	}

	newBlock->free = 0;

	return newBlock;
}

void correctFirstUsePtrM(struct page * targetPage, struct block * newBlock) {
	
	if (targetPage->pFirstUse == NULL) {
		targetPage->pFirstUse = newBlock;
	}
	else {
		struct block * blockPtr = targetPage->pFirstUse;
		while (blockPtr != newBlock) {
			blockPtr = blockPtr->pNext;
			if (blockPtr == NULL) {
				targetPage->pFirstUse = newBlock;
				break;
			}
		}
	}
}

void correctFirstUsePtrF(struct page * targetPage) {
	if (targetPage->pFirstUse->free == 1) {
		struct block * blockPtr = targetPage->pFirstUse;
		while (1) {
			blockPtr = blockPtr->pNext;
			if (blockPtr == NULL) {
				targetPage->pFirstUse = NULL;
				break;
			}
			else if (blockPtr->free == 0) {
				targetPage->pFirstUse = blockPtr;
				break;
			}
		}
	}
}

void correctFirstFreePtrM(struct page * targetPage) {

	if (targetPage->pFirstFree->free == 0) {
		struct block * blockPtr = targetPage->pFirstFree;
		while (1) {
			blockPtr = blockPtr->pNext;
			if (blockPtr == NULL) {
				targetPage->pFirstFree = NULL;
				break;
			}
			else if (blockPtr->free == 1) {
				targetPage->pFirstFree = blockPtr;
				break;
			}
		}
	}
}

void correctFirstFreePtrF(struct page * targetPage, struct block * freedBlock) {

	if (targetPage->pFirstFree == NULL) {
		targetPage->pFirstFree = freedBlock;
	}
	else {
		struct block * blockPtr = targetPage->pFirstFree;
		while (blockPtr != freedBlock) {
			blockPtr = blockPtr->pNext;
			if (blockPtr == NULL) {
				targetPage->pFirstFree = freedBlock;
				break;
			}
		}
	}
}

int findMaxFreeBlockSize(struct page * targetPage) {

	int maxSize = 0;

	if (targetPage->pFirstFree != NULL) {
		struct block * firstFree = targetPage->pFirstFree;
		maxSize = firstFree->szBlock;
		while (firstFree->pNext != NULL) {
			firstFree = firstFree->pNext;
			if (firstFree->free == 1 && firstFree->szBlock > maxSize) {
				maxSize = firstFree->szBlock;
			}
		}
	}

	return maxSize;
}

int findBlockOffset(struct page * targetPage, struct block * targetBlock) {

	struct block * blockPtr = findFirstBlock(targetPage);
	
	int offset = 0;

	while (blockPtr != targetBlock) {
		offset += blockPtr->szBlock;
		blockPtr = blockPtr->pNext;
		if (blockPtr == NULL) {
			return -1;
		}
	}

	return offset + targetPage->pageShift;
}

int mallocBlock(VA * ptr, int pageIdx, size_t szBlock ) {

	tick();

	if (szBlock < pageSize) {

		loadPage(pageIdx);

		struct block * newBlock;
		struct block * freeBlock = pages[pageIdx].pFirstFree;

		int err_code = findFirstFreeBlock(freeBlock, szBlock);
		if (err_code == -1) return 1;

		struct block * first = findFirstBlock(&pages[pageIdx]);
		newBlock = createAllocBlock(freeBlock, first, szBlock);

		correctFirstUsePtrM(&pages[pageIdx], newBlock);
		correctFirstFreePtrM(&pages[pageIdx]);

		pages[pageIdx].maxSizeFreeBlock = findMaxFreeBlockSize(&pages[pageIdx]);

		int offset = findBlockOffset(&pages[pageIdx], newBlock);
		if (offset == -1) return 1;
		newBlock->offset = offset;

		if (newBlock->pNext != NULL) {
			newBlock->pNext->offset = findBlockOffset(&pages[pageIdx], newBlock->pNext);
		}

		*(ptr) = generateVirtualAdress(pageIdx, offset);
		pages[pageIdx].info->age = 0;

		return 0;
	}
	else {
		int pagesRequired = szBlock / pageSize;

		struct block * newBlock = (struct block *) malloc(sizeof(struct block));
		newBlock->free = 0;
		newBlock->offset = 0;
		newBlock->pNext = NULL;
		newBlock->szBlock = szBlock;

		for (int idx = pageIdx; idx < pageIdx + pagesRequired; ++idx) {
			struct block * blockPtr = pages[idx].pFirstFree;
			free(blockPtr);
			pages[idx].maxSizeFreeBlock = 0;
			pages[idx].pFirstFree = NULL;
			if (pages[idx].info->age > 0) {
				pages[idx].info->age -= 1;
			}
			loadPage(idx);
		}

		if (szBlock % pageSize != 0) {
			int remainder = szBlock - pagesRequired * pageSize;
			pages[pageIdx + pagesRequired].maxSizeFreeBlock -= remainder;
			pages[pageIdx + pagesRequired].pageShift = remainder;
			pages[pageIdx + pagesRequired].info->age = 0;
			struct block * firstBlock = pages[pageIdx + pagesRequired].pFirstFree;
			firstBlock->szBlock -= remainder;
			firstBlock->offset = findBlockOffset(&pages[pageIdx + pagesRequired], firstBlock);
			loadPage(pageIdx + pagesRequired);
		}

		pages[pageIdx].pFirstUse = newBlock;
		*(ptr) = generateVirtualAdress(pageIdx, 0);

		return 0;
	}
}

int _malloc(VA * ptr, size_t szBlock) {

	if (szBlock < 1) return -1;

	int targetPageIdx = -1;

	for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
		if (pages[pageIdx].maxSizeFreeBlock >= szBlock) {
			if (pages[pageIdx].info->inUse == 1) {
				targetPageIdx = pageIdx;
				break;
			}
			else if (pages[pageIdx].info->inUse == 0 && targetPageIdx == -1) {
				targetPageIdx = pageIdx;
			}
		}
		else if (szBlock > pageSize && pages[pageIdx].pFirstUse == NULL && pages[pageIdx].pFirstFree != NULL &&
			pages[pageIdx].pFirstFree->offset == 0) {
			int pagesRequired = szBlock / pageSize;
			char suitable = 1;
			for (int idx = pageIdx + 1; idx < pageIdx + 1 + pagesRequired; ++idx) {
				if (idx >= numOfPages || pages[idx].pFirstUse != NULL || pages[idx].pFirstFree->offset != 0) {
					suitable = 0;
					break;
				}
			}

			if (suitable == 1) {
				if (pages[pageIdx].info->inUse == 1) {
					targetPageIdx = pageIdx;
					break;
				}
				else if (pages[pageIdx].info->inUse == 0 && targetPageIdx == -1) {
					targetPageIdx = pageIdx;
				}
			}
		}
	}

	if (targetPageIdx == -1) {
		return -2;
	}

	return mallocBlock(ptr, targetPageIdx, szBlock);
}

struct block * findBlockByOffset(struct page * targetPage, int blockOffset) {
	
	struct block * blockPtr = findFirstBlock(targetPage);

	while (blockPtr->offset + blockPtr->szBlock - 1 < blockOffset) {
		blockPtr = blockPtr->pNext;
	}

	return blockPtr;
}

int uniteFreeBlocks(struct page * targetPage) {

	struct block * blockPtr = targetPage->pFirstFree;

	if (blockPtr == NULL) return -1;

	while (blockPtr->pNext != NULL) {
		while (blockPtr->free == 1 && blockPtr->pNext->free == 1) {
			struct block * temp = blockPtr->pNext;
			blockPtr->pNext = temp->pNext;
			blockPtr->szBlock += temp->szBlock;
			free(temp);
			if (blockPtr->pNext == NULL) return 0;
		}
		blockPtr = blockPtr->pNext;
	}

	return 0;
}

VA virtualToPhysical(VA ptr, size_t szBuffer, int * pageNumber, int * blockSize, int * blockInnerOffset, int * err_code) {

	if (ptr == NULL) {
		*(err_code) = -1;
		return NULL;
	}

	int pageNum = 0;
	int blockOffset = 0;

	virtualToBlockParams(ptr, &pageNum, &blockOffset);

	if (pageNum >= numOfPages) {
		*(err_code) = -2;
		return NULL;
	}

	*(blockInnerOffset) = blockOffset;

	struct block * targetBlock = findBlockByOffset(&pages[pageNum], blockOffset);
	blockOffset = findBlockOffset(&pages[pageNum], targetBlock);

	*(blockInnerOffset) -= blockOffset;

	if (targetBlock->szBlock < szBuffer) {
		*(err_code) = -2;
		return NULL;
	}

	loadPage(pageNum);
	int pageOffset = pages[pageNum].info->offsetPage;

	VA physAdd = physicMem + pageOffset * pageSize + blockOffset;
	*(err_code) = 0;
	*(pageNumber) = pageNum;
	*(blockSize) = targetBlock->szBlock;

	return physAdd;
}

int findPageIdxByOffset(int offset) {

	for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
		if (pages[pageIdx].info->offsetPage == offset) {
			return pageIdx;
		}
	}

	return -1;
}

int _write(VA ptr, void* pBuffer, size_t szBuffer) {
	
	if (pBuffer == NULL || szBuffer < 1) {
		return -1;
	}

	tick();

	int err_code = 0;
	int pageNumber = 0;
	int szBlock = 0;
	int blockInnerOffset = 0;

	VA buffer = (VA)pBuffer;
	VA physAdd = virtualToPhysical(ptr, szBuffer, &pageNumber, &szBlock, &blockInnerOffset, &err_code);

	if (err_code != 0) {
		return err_code;
	}

	if (szBuffer > szBlock - blockInnerOffset) {
		return -2;
	}

	if (pages[pageNumber].info->age > 0) {
		pages[pageNumber].info->age -= 1;
	}
	pages[pageNumber].info->modified = 1;

	int idx = blockInnerOffset;
	for (int cellIdx = 0; cellIdx < szBuffer; ++cellIdx) {
		physAdd[idx] = buffer[cellIdx];
		idx++;
		if (idx >= pageSize) {
			idx = 0;
			pageNumber++;

			loadPage(pageNumber);
			int pageOffset = pages[pageNumber].info->offsetPage;
			physAdd = physicMem + pageOffset*pageSize;

			if (pages[pageNumber].info->age > 0) {
				pages[pageNumber].info->age -= 1;
			}
			pages[pageNumber].info->modified = 1;
		}
	}

	return 0;
}

VA moveToNextPage(int * curPageNumber) {
	
	if (*(curPageNumber) >= numOfPages) {
		return NULL;
	}

	++*(curPageNumber);

	loadPage(*(curPageNumber));
	int pageOffset = pages[*(curPageNumber)].info->offsetPage;
	VA physAdd = physicMem + pageOffset*pageSize;

	return physAdd;
}

int _read(VA ptr, void* pBuffer, size_t szBuffer) {

	if (pBuffer == NULL || szBuffer < 1) {
		return -1;
	}

	tick();

	int err_code = 0;
	int sourcePageNumber = 0;
	int targetPageNumber = 0;
	int szBlock = 0;
	int sourceBlockInnerOfsset = 0;
	int targetBlockInnerOfsset = 0;

	VA physAdd = virtualToPhysical(ptr, szBuffer, &sourcePageNumber, &szBlock, &sourceBlockInnerOfsset, &err_code);
	if (err_code != 0) {
		return err_code;
	}

	tick();
	pages[sourcePageNumber].info->age = 0;

	VA buffer = virtualToPhysical((VA)pBuffer, szBuffer, &targetPageNumber, &szBlock, &targetBlockInnerOfsset, &err_code);
	if (err_code != 0) {
		return err_code;
	}

	if (szBuffer > szBlock - sourceBlockInnerOfsset ||
		szBuffer > szBlock - targetBlockInnerOfsset) {
		return -2;
	}

	pages[sourcePageNumber].info->age = 0;
	pages[targetPageNumber].info->age = 0;
	pages[targetPageNumber].info->modified = 1;

	printPages();
	
	int sourceIdx = sourceBlockInnerOfsset;
	int targetIdx = targetBlockInnerOfsset;
	for (int cellIdx = 0; cellIdx < szBuffer; ++cellIdx) {
		buffer[targetIdx] = physAdd[sourceIdx];
		sourceIdx++;
		targetIdx++;
		if (sourceIdx >= pageSize) {
			sourceIdx = 0;
			tick();
			pages[sourcePageNumber].info->age = 0;
			physAdd = moveToNextPage(&sourcePageNumber);
			printPages();
		}

		if (targetIdx >= pageSize) {
			targetIdx = 0;
			tick();
			pages[targetPageNumber].info->modified = 1;
			pages[sourcePageNumber].info->age = 0;
			buffer = moveToNextPage(&targetPageNumber);
			printPages();
		}
	}
	return 0;
}

int _free(VA ptr) {

	int pageNum = 0;
	int blockOffset = 0;

	virtualToBlockParams(ptr, &pageNum, &blockOffset);

	if (pageNum >= numOfPages || blockOffset > pageSize) {
		return -1;
	}

	struct block * targetBlock = findBlockByOffset(&pages[pageNum], blockOffset);

	if (targetBlock->szBlock <= pageSize) {

		loadPage(pageNum);
		targetBlock->free = 1;
		correctFirstUsePtrF(&pages[pageNum]);
		correctFirstFreePtrF(&pages[pageNum], targetBlock);

		uniteFreeBlocks(&pages[pageNum]);
		pages[pageNum].maxSizeFreeBlock = findMaxFreeBlockSize(&pages[pageNum]);

		tick();
		if (pages[pageNum].info->age > 0) {
			pages[pageNum].info->age -= 1;
		}
	}
	else {
		int pagesRequired = targetBlock->szBlock / pageSize;
		
		tick();

		for (int pageIdx = pageNum; pageIdx < pageNum + pagesRequired; ++pageIdx) {
			pages[pageIdx].pFirstUse = NULL;
			pages[pageIdx].pFirstFree = createEmptyBlock(pageSize);
			pages[pageIdx].maxSizeFreeBlock = pageSize;
			if (pages[pageIdx].info->age > 0) {
				pages[pageIdx].info->age -= 1;
			}
			loadPage(pageIdx);
		}

		if (targetBlock->szBlock % pageSize != 0) {
			int remainder = targetBlock->szBlock - pagesRequired * pageSize;
			struct block * firstBlock = findFirstBlock(&pages[pageNum + pagesRequired]);
			struct block * emptyBlock = createEmptyBlock(remainder);
			pages[pageNum + pagesRequired].pFirstFree = emptyBlock;
			emptyBlock->pNext = firstBlock;
			uniteFreeBlocks(&pages[pageNum + pagesRequired]);
			pages[pageNum + pagesRequired].pageShift = 0;
			pages[pageNum + pagesRequired].maxSizeFreeBlock = findMaxFreeBlockSize(&pages[pageNum + pagesRequired]);
			pages[pageNum + pagesRequired].info->age = 0;
			loadPage(pageNum + pagesRequired);
		}
	}

	return 0;
}

void clearPageBlockInfo(struct page * targetPage) {

	struct block * blockPtr = findFirstBlock(targetPage);
	while (blockPtr != NULL) {
		struct block * temp = blockPtr;
		blockPtr = blockPtr->pNext;
		free(temp);
	}
}

int _clear() {

	if (initialized != 0) {

		free(physicMem);
		free(driveMem);

		for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
			clearPageBlockInfo(&pages[pageIdx]);
			free(pages[pageIdx].info);
		}

		free(pages);

		numOfPages = 0;
		pageSize = 0;
		initialized = 0;
		shift = 0;

		return 0;
	}
	return -1;
}

int printPage(struct page * targetPage) {

	printf("In use: %d Max free block: %d Age: %d Modified: %d Offset: %d\n",
		targetPage->info->inUse,
		targetPage->maxSizeFreeBlock,
		targetPage->info->age,
		targetPage->info->modified,
		targetPage->info->offsetPage);

	struct block * blockPtr = findFirstBlock(targetPage);

	if (blockPtr == NULL) {
		printf("[NO BLOCKS FOUND]\n");
		return -1;
	}

	while (1) {
		printf("%d %d %d", blockPtr->szBlock, blockPtr->offset, blockPtr->free);
		blockPtr = blockPtr->pNext;
		if (blockPtr == NULL) break;
		printf(" -> ");
	}

	printf("\n");
	return 0;
}

int printPages() {

	printf("Memory structure: \n");

	for (int pageIdx = 0; pageIdx < numOfPages; ++pageIdx) {
		printf("Page: %d ", pageIdx);
		printPage(&pages[pageIdx]);
	}

	printf("\n");
	return 0;
}

void printPageFaults() {
	printf("Page faults: %d\n", pageFaults);
}

void printPageTableSize() {

	printf("Table size: %d\n", numOfPages * (sizeof(struct page) + sizeof(struct pageInfo)));
}

void printAddrCalculationTime() {
	printf("Address calculation time(only on Windows): %f\n", addrCalculationTime);
}

double getAddrCalculationTime() {
	return addrCalculationTime;
}

void resetAddrCalculationTime() {
	addrCalculationTime = 0;
}

void resetPageFaults() {
	pageFaults = 0;
}

int getPageFaults() {
	return pageFaults;
}

int printMemory(VA ptr, int amount) {

	if (amount < 1) {
		return -1;
	}

	tick();

	int err_code = 0;
	int pageNumber = 0;
	int szBlock = 0;
	int blockInnerOfsset = 0;

	VA physAdd = virtualToPhysical(ptr, amount, &pageNumber, &szBlock, &blockInnerOfsset, &err_code);

	if (err_code != 0) {
		return err_code;
	}

	if (amount > szBlock - blockInnerOfsset) {
		return -2;
	}

	pages[pageNumber].info->age = 0;

	int idx = blockInnerOfsset;
	for (int cellIdx = 0; cellIdx < amount; ++cellIdx) {
		printf("%c", physAdd[idx]);
		idx++;
		if (idx >= pageSize) {
			idx = 0;
			physAdd = moveToNextPage(&pageNumber);
		}
	}
	return 0;
}
