#include "mmemory.h"

void printCharArray(char * arr, int size) {

	for (int index = 0; index < size; ++index) {
		printf("%c", arr[index]);
	}
}

void printIntArray(int * arr, int size) {

	for (int index = 0; index < size; ++index) {
		printf("%d ", arr[index]);
	}
}

int * randIntArray(int size) {

	int * arr = (int *)malloc(size * sizeof(int));

	srand(time(NULL));

	for (int index = 0; index < size; ++index) {
		arr[index] = rand() % 10;
	}

	return arr;
}

void writeIntArrayToCSV(int * arr, int size, char * filename) {

	FILE * file;
	char * mode = "w+";

	file = fopen(filename, mode);

	for (int valueIdx = 0; valueIdx < size; ++valueIdx) {
		fprintf(file, "%d;", arr[valueIdx]);
	}

	fclose(file);
}

void writeDoubleArrayToCSV(double * arr, int size, char * filename) {

	FILE * file;
	char * mode = "w+";

	file = fopen(filename, mode);

	for (int valueIdx = 0; valueIdx < size; ++valueIdx) {
		fprintf(file, "%f;", arr[valueIdx]);
	}

	fclose(file);
}

void appendIntArrayToCSV(int * arr, int size, char * filename) {

	FILE * file;
	char * mode = "a+";

	file = fopen(filename, mode);
	fprintf(file, "\n");

	for (int valueIdx = 0; valueIdx < size; ++valueIdx) {
		fprintf(file, "%d;", arr[valueIdx]);
	}

	fclose(file);
}

char * readChars(int number, char * filename) {

	FILE * file;
	char * mode = "r";

	char * buffer = (char *)malloc(number);

	file = fopen(filename, mode);
	fgets(buffer, number, file);

	return buffer;
}


/*
��������� ����� �������� ������ ��������
*/
void test1(int pageSize) {

	_init(DRIVE_MEM_SIZE / pageSize + 1, pageSize);

	VA addr = NULL;
	VA addr2 = NULL;
	VA addr3 = NULL;
	VA addr4 = NULL;

	char * testData = readChars(171, "resources/shakespeare.txt");
	char testData2[] = "Solmates never die";

	int err_code = _malloc(&addr, 171);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _malloc(&addr2, 18);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _malloc(&addr3, 171);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _malloc(&addr4, 18);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _write(addr, testData, 171);
	printf("Write finished with error code %d \n", err_code);

	err_code = _write(addr2, testData2, 18);
	printf("Write finished with error code %d \n", err_code);

	err_code = _read(addr, addr3, 171);
	printf("Read finished with error code %d \n", err_code);

	err_code = _read(addr2, addr4, 18);
	printf("Read finished with error code %d \n", err_code);

	printMemory(addr3, 171);
	printf("\n");
	printMemory(addr4, 18);

	err_code = _free(addr);
	printf("\nFree finished with error code %d \n", err_code);
	err_code = _free(addr2);
	printf("\nFree finished with error code %d \n", err_code);
	err_code = _free(addr3);
	printf("\nFree finished with error code %d \n", err_code);
	err_code = _free(addr4);
	printf("\nFree finished with error code %d \n", err_code);

	_clear();

	free(testData);
}

/*
��������� 20 ����, ������ 18-��, ������ � 10-�� �� 15-�
*/
void test2() {

	_init(8, 64);

	VA addr = NULL;
	VA addr2 = NULL;
	char testData[] = "Solmates never die";

	int err_code = _malloc(&addr, 20);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _malloc(&addr2, 5);
	printf("Malloc finished with error code %d \n", err_code);
	printPages();

	err_code = _write(addr, testData, 18);
	printf("Write finished with error code %d \n", err_code);

	err_code = _read(addr + 9, addr2, 5);
	printf("Read finished with error code %d \n", err_code);

	printf("\n");
	printMemory(addr2, 5);

	err_code = _free(addr);
	printf("\nFree finished with error code %d \n", err_code);

	_clear();
}

void pageTest() {

	int pageSz = 64;
	int swapNum[20];
	int pageSize[20];

	for (int testIdx = 0; testIdx < 20; ++testIdx) {
		test1(pageSz);

		pageSize[testIdx] = pageSz;
		swapNum[testIdx] = getPageFaults();
		resetPageFaults();
		pageSz -= 2;
	}

	writeIntArrayToCSV(pageSize, 20, "output/pageTest.csv");
	appendIntArrayToCSV(swapNum, 20, "output/pageTest.csv");

}

void timeTest() {

	int iterations = 5;
	double values[20];
	int iterationArr[20];

	for (int testIdx = 0; testIdx < 20; ++testIdx) {
		for (unsigned int iteration = 0; iteration < iterations; ++iteration) {
			test2();
		}
		values[testIdx] = getAddrCalculationTime();
		iterationArr[testIdx] = iterations;
		resetAddrCalculationTime();
		iterations += 5;
	}

	writeDoubleArrayToCSV(values, 20, "output/timeTest.csv");
	appendIntArrayToCSV(iterationArr, 20, "output/timeTest.csv");
}

void memoryTest() {
	
	//test1(32);

	timeTest();
	pageTest();
}