#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define PHYSIC_MEM_SIZE 256
#define DRIVE_MEM_SIZE 512

#ifdef _WIN32
#include <windows.h>
#endif

#if _MSC_VER >= 1700
#pragma warning(disable : 4996)
#endif


void memoryTest();

